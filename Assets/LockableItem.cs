﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockableItem : MonoBehaviour {

	public enum LockState {
		NONE, LOCKED, CONFIRM
	};

	public WeaponManager weaponManager;
	public LockState state = LockState.NONE;
	public AudioSource soundLock;

	public CrosshairController crosshair;

	// Use this for initialization
	void Start () {
		weaponManager = GameObject.FindGameObjectWithTag ("WeaponManager").GetComponent<WeaponManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LockYourself(){
		if (weaponManager.LockItem (this)) {
			// lock success
			//GetComponent<Renderer>().material.color = Color.green;
			state = LockState.LOCKED;
			crosshair.showLocked ();
			soundLock.Play ();
		}
	}

	public void SetLockConfirm(){
		//GetComponent<Renderer>().material.color = Color.red;
		state = LockState.CONFIRM;
		crosshair.showConfirm ();
	}

	public void Unlock(){
		if (state != LockState.NONE) {
			//GetComponent<Renderer> ().material.color = Color.white;
			if (state == LockState.LOCKED) {
				weaponManager.RemoveFromLock (this);
			}
			state = LockState.NONE;
			crosshair.showDetect ();
		}
	}
}
