﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacePlayer : MonoBehaviour {

	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("PlayerCenter");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerDir = player.transform.position - transform.position;
		transform.rotation = Quaternion.LookRotation (playerDir);
	}
}
