﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncPositionWith : MonoBehaviour {

	public GameObject target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
			transform.position = target.transform.position;
		}
	}
}
