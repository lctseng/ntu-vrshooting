﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArmorController : MonoBehaviour {

	public float attackInterval = 1.0f;
	private float attackTime = 0.0f;

	public GameObject bulletCandidate;

	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("PlayerCenter");
		attackTime = attackInterval;
		
	}
	
	// Update is called once per frame
	void Update () {
		attackTime -= Time.deltaTime;
		if (attackTime < 0) {
			attackTime = attackInterval + Random.value * attackInterval;
			FireWeapon ();
		}
	}

	void FireWeapon(){
		var bullet = Instantiate (bulletCandidate);
		bullet.transform.position = transform.position;
		bullet.GetComponent<EnemyBulletController> ().SetDirection(player.transform.position - transform.position);
	}
}
