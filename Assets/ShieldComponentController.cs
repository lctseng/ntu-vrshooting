﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShieldComponentController : MonoBehaviour {

	public ShieldController shield;
	public Image hitAlertImage;

	Tweener tweenAnimation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Hit(int damage){
		shield.OnShieldHit (this.gameObject, damage);
		PlayHitAnimation ();
	}

	void PlayHitAnimation(){
		if (tweenAnimation != null) {
			tweenAnimation.Kill ();
		}
		hitAlertImage.color = Color.white;
		tweenAnimation = DOTween.To (()=>hitAlertImage.color, (x) => hitAlertImage.color = x, new Color(1,1,1,0), 0.5f);
	}
}
