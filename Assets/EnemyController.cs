﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {


	private GameObject player;
	private Rigidbody rigidBody;
	public float sqrDistnace = 1.0f;
	public float approachFactor = 1.0f;
	public float rotateFactor = 20.0f;
	public float sqrDetectDistance = 36.0f;
	public bool destroyParent = false;
	private GameObject enemyManager;


	public int suicideDamage = 5;

	public GameObject explosion;

	public CrosshairController crosshair;


	private Vector3 rotateAxis;



	// Use this for initialization
	void Start () {
		enemyManager = GameObject.FindGameObjectWithTag ("EnemyManager");
		player = GameObject.FindGameObjectWithTag ("PlayerCenter");
		rigidBody = GetComponent<Rigidbody> ();
		Vector3 playerDir = player.transform.position - transform.position;
		transform.rotation = Quaternion.LookRotation (playerDir);
		rotateAxis = Vector3.Cross (transform.right, playerDir);
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround(player.transform.position, rotateAxis, rotateFactor * Time.deltaTime);
		// approach player
		Vector3 playerDir = player.transform.position - transform.position;
		float playerDis = Vector3.SqrMagnitude (playerDir);
		if (playerDis > sqrDistnace) {
			rigidBody.velocity = playerDir * approachFactor;
		} else {
			rigidBody.velocity = Vector3.zero;
		}
		if (playerDis < sqrDetectDistance) {
			crosshair.showDetect ();
		}
	}

	public void Hit(int damage){
		OnDie ();
	}

	void OnDie(){
		GetComponent<LockableItem>().Unlock();
		explosion.transform.parent = null;
		explosion.SetActive (true);
		enemyManager.SendMessage ("AddScore", 1, SendMessageOptions.DontRequireReceiver);
		if (destroyParent) {
			Destroy (this.gameObject.transform.parent.gameObject);
		} else {
			Destroy (this.gameObject);
		}

	}



	void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("Player")){
			other.SendMessage ("Hit", suicideDamage, SendMessageOptions.DontRequireReceiver);
			OnDie ();
		}
	}

}
