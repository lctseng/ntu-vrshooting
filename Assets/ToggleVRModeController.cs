﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleVRModeController : MonoBehaviour {

	public GvrViewer viewer;

	public void ToggleVRMode(){
		viewer.VRModeEnabled = !viewer.VRModeEnabled;
	}
}
