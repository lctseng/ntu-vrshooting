﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairController : MonoBehaviour {


	public GameObject effectLocked;
	public GameObject effectConfirm;
	public GameObject effectDetect;

	private int showLevel = 0;

	public void hide(){
		effectLocked.SetActive (false);
		effectConfirm.SetActive (false);
		effectDetect.SetActive (false);
		showLevel = 0;
	}

	public void showLocked(){
		effectLocked.SetActive (true);
		effectConfirm.SetActive (false);
		effectDetect.SetActive (false);
		showLevel = 2;
	}

	public void showConfirm(){
		effectLocked.SetActive (false);
		effectConfirm.SetActive (true);
		effectDetect.SetActive (false);
		showLevel = 2;
	}

	public void showDetect(){
		if (showLevel < 1) {
			effectLocked.SetActive (false);
			effectConfirm.SetActive (false);
			effectDetect.SetActive (true);
			showLevel = 1;
		}
	}
}
