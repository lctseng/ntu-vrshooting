﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletController : MonoBehaviour {

	public float flySpeed = 1.0f;
	public float lifeTime = 1.0f;

	public GameObject explosion;

	// Use this for initialization
	void Start () {
		Invoke ("KillYourself", lifeTime);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void SetDirection(Vector3 direction){
		var rigidBody = GetComponent<Rigidbody> ();
		rigidBody.velocity = direction * flySpeed;
		transform.rotation = Quaternion.LookRotation (rigidBody.velocity);
	}

	void KillYourself(){
		explosion.transform.parent = null;
		explosion.SetActive (true);
		Destroy (this.gameObject);
	}

	void OnTriggerEnter(Collider other) {
		other.SendMessage ("Hit", 1, SendMessageOptions.DontRequireReceiver);
		KillYourself ();
	}
}
