﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using ProgressBar;
using UnityEngine.SceneManagement;

public class HullController : MonoBehaviour {

	public Image hitAlertImage;
	public int maxHp = 100;
	public int currentHp;
	public ProgressBarBehaviour hpMeter;
	public bool die = false;
	public GameObject explosion;
	public Image dieImage;

	public AudioSource soundDamage;

	Tweener tweenAnimation;

	// Use this for initialization
	void Start () {
		currentHp = maxHp;	
		OnHpValueChanged ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Hit(int damage){
		if (!die) {
			PlayHitAnimation ();
			currentHp -= damage;
			soundDamage.Play ();
			OnHpValueChanged ();
			if (currentHp <= 0) {
				// Die!
				die = true;
				explosion.SetActive (true);
				DOTween.To (()=>dieImage.color, (x)=>dieImage.color = x, new Color(0,0,0,1), 1.0f).SetDelay(3).OnComplete(()=>{
					ResetScene();
				});
			}
		}

	}

	public void ResetScene(){
		SceneManager.LoadScene("Space");		
	}

	void PlayHitAnimation(){
		if (tweenAnimation != null) {
			tweenAnimation.Kill ();
		}
		hitAlertImage.color = Color.white;
		tweenAnimation = DOTween.To (()=>hitAlertImage.color, (x) => hitAlertImage.color = x, new Color(1,1,1,0), 0.5f);
	}

	void OnHpValueChanged ()
	{
		hpMeter.Value = Mathf.Clamp(currentHp * 100.0f / maxHp, 0.0f, 100.0f);
	}
}
