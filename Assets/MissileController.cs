﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour {

	private LockableItem targetLockable;
	public float initSpeed = 0.5f;
	public float moveSpeed = 0.1f;
	public float forceFactor = 0.1f;
	public float startTrackingTime = 0.5f;
	public float turningForceFactor = 0.5f;
	public float dashForceFactor = 1.0f;
	public float lifeTime = 3.0f;

	public int maxHp = 2;
	private int currentHp;

	private Rigidbody rigidBody;
	public GameObject explosion;

	// Use this for initialization
	void Start () {
		rigidBody = this.GetComponent<Rigidbody> ();
		rigidBody.velocity = transform.forward * initSpeed;
		currentHp = maxHp;
		Invoke ("KillSelf", lifeTime);
	}
	
	// Update is called once per frame
	void Update () {
		if (startTrackingTime < 0.0f) {
			if (targetLockable != null) {
				Vector3 targetDir = (targetLockable.gameObject.transform.position - transform.position).normalized;
				float angleBetween = Vector3.Angle (transform.forward, targetDir);
				Vector3 forceDir;
				if (angleBetween > 30) {
					Vector3 fakeTargetDir = Vector3.Slerp (transform.forward, targetDir, 0.6f);
					forceDir = fakeTargetDir - transform.forward;
					rigidBody.AddForce (forceDir * forceFactor * turningForceFactor);
					
				} else {
					forceDir = targetDir - transform.forward;
					rigidBody.AddForce (targetDir * forceFactor * (180.0f - angleBetween) / 180.0f * dashForceFactor);
					rigidBody.AddForce (forceDir * forceFactor * turningForceFactor * rigidBody.velocity.magnitude);

				}


				transform.rotation = Quaternion.LookRotation (rigidBody.velocity);

			} else {
				// target lost
				Invoke("KillSelf", 1);
			}

		} else {
			startTrackingTime -= Time.deltaTime;
		}
	}

	void OnTriggerEnter(Collider other) {
		other.SendMessage ("Hit", 0, SendMessageOptions.DontRequireReceiver);
		Explode ();
	}

	public void SetTarget(LockableItem target){
		this.targetLockable = target;
	}

	void KillSelf(){
		Explode ();
	}

	void Explode(){
		if (targetLockable != null) {
			targetLockable.Unlock ();
		}
		explosion.transform.parent = null;
		explosion.SetActive (true);
		Destroy(this.gameObject);
	}

	// being hitted
	void Hit(int damage){
		currentHp -= damage;
		if (currentHp <= 0) {
			Explode ();
		}
	}
}
