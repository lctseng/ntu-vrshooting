﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ProgressBar;

public class ShieldController : MonoBehaviour {

	public HullController hull;
	public int shieldValue;
	public float shieldRecoverInterval = 1.0f;
	public int shieldRecoverValue = 1;
	public int shieldMax = 100;
	public ProgressBarBehaviour shieldMeter;

	public AudioSource soundShield;

	private float shieldRecoverTime;

	// Use this for initialization
	void Start () {
		shieldValue = shieldMax;
		OnShieldValueChanged ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!hull.die) {
			shieldRecoverTime += Time.deltaTime;
			if (shieldRecoverTime >= shieldRecoverInterval) {
				shieldRecoverTime = 0;
				shieldValue += shieldRecoverValue;
				if (shieldValue > shieldMax) {
					shieldValue = shieldMax;
				}
				OnShieldValueChanged ();

			}
		}
	}

	public void OnShieldHit(GameObject component, int damage){
		if (shieldValue > 0) {
			shieldValue -= damage;
			OnShieldValueChanged ();
			soundShield.Play ();
		} else {
			// damage hull!
			hull.Hit(damage);
		}
	}

	void OnShieldValueChanged(){
		shieldMeter.Value = Mathf.Clamp (shieldValue * 100.0f / shieldMax, 0.0f, 100.0f);
	}
}
