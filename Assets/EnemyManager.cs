﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour {

	public GameObject[] enemyCandidates;

	public float spawnInterval = 5.0f;
	public float spawnDistance = 10.0f;
	public int score = 0;

	public Text scoreText;

	private float spawnCount = 0;

	// Use this for initialization
	void Start () {
		spawnCount = spawnInterval;
	}
	
	// Update is called once per frame
	void Update () {
		spawnCount += Time.deltaTime;
		if (spawnCount > spawnInterval) {
			int num = 1 + score / 5;
			for (int i = 0; i < num; i++) {
				SpawnEnemy ();	
			}
			spawnCount = 0;
		}
	}

	void SpawnEnemy(){
		int index = Random.Range (0, enemyCandidates.Length);
		var enemy = Instantiate (enemyCandidates [index]);
		enemy.transform.position = Random.onUnitSphere * spawnDistance;
	}

	void AddScore(int value){
		score += value;
		scoreText.text = "Score: " + score;
	}
}
