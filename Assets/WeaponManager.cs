﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProgressBar;
using UnityEngine.UI;

public class WeaponManager : MonoBehaviour {

	public HashSet<LockableItem> lockedItems;
	public GameObject missileCandidate;
	public int maxLockableItem = 5;
	public Text lockNumberText;

	public float weaponChargeTime = 5.0f;
	private float weaponChargeTimeCurrent = 0.0f;


	public ProgressRadialBehaviour weaponMeter;

	// Use this for initialization
	void Start () {
		lockedItems = new HashSet<LockableItem>();
		updateLockNumberText ();
		MagnetSensor.OnCardboardTrigger += TryLaunchWeapon;
	}
	
	// Update is called once per frame
	void Update () {
		
		weaponChargeTimeCurrent += Time.deltaTime;
		if (weaponChargeTimeCurrent > weaponChargeTime) {
			weaponChargeTimeCurrent = weaponChargeTime;
		}
		weaponMeter.Value = Mathf.Clamp (weaponChargeTimeCurrent * 100.0f / weaponChargeTime,0.0f, 100.0f);
		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
		#else
		if (Input.touchCount > 0)
		#endif
		{
			TryLaunchWeapon ();
		}

	}

	void TryLaunchWeapon(){
		if (lockedItems.Count > 0 && weaponChargeTimeCurrent >= weaponChargeTime) {
			LaunchWeapon ();
			weaponChargeTimeCurrent = 0;
			lockedItems.Clear ();
			updateLockNumberText ();
		}
	}

	public bool LockItem(LockableItem target){
		if (lockedItems.Count < maxLockableItem && target.state == LockableItem.LockState.NONE) {
			lockedItems.Add (target);
			updateLockNumberText ();
			return true;
		} else {
			return false;
		}
	}

	void LaunchWeapon(){
		foreach(LockableItem target in lockedItems){
			if (target != null) {
				target.SetLockConfirm ();
				var missile = Instantiate (missileCandidate);
				missile.transform.position = transform.position + Random.insideUnitSphere * 0.1f;
				missile.transform.rotation = transform.rotation;
				missile.GetComponent<MissileController> ().SetTarget (target);
			}

		}
	}

	public void RemoveFromLock(LockableItem target){
		Debug.Log ("Removing Lock");
		lockedItems.Remove (target);
		updateLockNumberText ();
	}

	void updateLockNumberText(){
		lockNumberText.text = string.Format ("{0} / {1}",lockedItems.Count,maxLockableItem);
	}
}
